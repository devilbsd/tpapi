import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY
from api_config import API_COOKIE as API_COOKIE


report_uid = "b6d0f346-69a6-4859-a5da-3e9324987fc5"

print '\nDownloaing Sanitized File from TEX Scrubbing\n'


response = requests.get( API_URL + 'download',
                          headers = {'Authorization': API_KEY },
						  params = {"id": report_uid},
						  cookies = API_COOKIE, ## This value will change!!
                          verify=False)


print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response

filename = "converted.pdf"

with open (filename, mode = 'wb' ) as reportfile:
	reportfile.write(response.content)


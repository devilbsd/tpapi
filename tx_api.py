'''
WARNING
THIS IS AN SCRIPT FOR THE FIRST VERSIONOF THE API
I HAVE NO IDEA HOW RELEVANT IS THIS INFORMATION
BUT THE SCRIPT DOES NOT WORK ANYMORE
'''


#- Threat Prevention API Example. Threat Extraction Only
import json
import requests
import base64
import string
import os
import hashlib

cloud_url = 'https://192.168.80.254/UserCheck/TPAPI'
scrubbed_parts_codes = [1034, 1026, 1019,1018, 1039, 1042, 1043, 1036, 1037, 1143, 1141, 1150, 1151, 1137, 1017, 1021]
origfile = 'documents/macrofile.doc'

def fileencode(originalfile=None):
        '''Encode a file with base64 so it can be transferered with the POST Request
        original_file defaults to None but needs an argument, this will be the full path
        or just the name of the file if it is on the same diectory'''
        with open(originalfile) as f:
                encoded = base64.b64encode(f.read())
                return encoded
            
def getsha1(shafile=None):
    '''Ruturns the SHA1 hash from a file. It will work for big and small files.
    They will be read in 128 bytes chunks, so the hashing will happen for a stream
    of chunks, no need to allocate the entire file on RAM.
    shafile is the full or name of the file, defauls to None and is mandatory, '''
    with open(shafile,'r') as f:
        file_hash = hashlib.sha1()
        while True:
            data =  f.read(2**20)
            if not data:
                break
            file_hash.update(data)   
        return file_hash.hexdigest()
                    

def filedecode(encodedscrubed=None, output_filename=None):
        '''Decode a Base64 encoded stream to the scrubbed file retuned by theTX Scrubbing Center'''
        with open(output_filename, 'w') as f:
                data = base64.b64decode(encodedscrubed)
                f.write(data)
        
def sendApiCmd(file_enc_data=None, file_orig_name=None):
        '''Construct the POST request and send it to the TP API Scrub Service'''
        apiRequest = {"request": [{"protocol_version": '1.2',
                                   "request_name" : 'UploadFile',
                                   "file_orig_name": file_orig_name,
                                   "file_enc_data": file_enc_data,
                                   "sender": "jpadilla@checkpoint.com",
                                   "recipients": "jpadilla@checkpoint.com",
                                   "subject": 'Your scrubbed file is %s' % origfile,
                                   "src_ip": "192.168.206.129",
                                   "file_orig_sha1": getsha1(shafile=origfile),
                                   "file_orig_size": os.path.getsize(file_orig_name),
                                    #0:GW policy, 1:Clean, 2:PDF 3:TIFF 4:MHTML
                                   "scrub_options" : {"scrub_method": 3,
                                                      "scrubbed_parts_codes": scrubbed_parts_codes},
                                        }]}
        
        print '=> API Request ------------------------------------------'
        print '=> Request name: ', apiRequest['request'][0]['request_name']
        print '=> SHA1 on request: ', apiRequest['request'][0]['file_orig_sha1']
        print '=> Size   : ', apiRequest['request'][0]['file_orig_size']
        print '=> Subject: ', apiRequest['request'][0]['subject']
        print '=> Encoded Data: ', apiRequest['request'][0]['file_enc_data'][0:50]
        req = requests.post(cloud_url, data = json.dumps(apiRequest), verify=False)
        requestResponse = json.loads(req.text) 
        return requestResponse


#Encode file in Base64
fileb64 = fileencode(origfile)

#Send the Request and print results
response = sendApiCmd(file_enc_data=fileb64, file_orig_name=origfile)
#- Enable the followingline to print the encoded base64 file
print response['response'][0]
print '=> API Response ---------------------------------------------------------------------'
print '=> Scrubbed Result : ', response['response'][0]['scrub']['scrub_result']
print '=> Scrubbed Content: ', response['response'][0]['scrub']['scrubbed_content']
print '=> Message    : ', response['response'][0]['scrub']['message']
print '=> Scrub Time : ', response['response'][0]['scrub']['scrub_time']
print '=> Output File: ', response['response'][0]['scrub']['output_file_name']

#Save encoded respose to a file
filedecode(encodedscrubed=response['response'][0]['scrub']['file_enc_data'], 
           output_filename=response['response'][0]['scrub']['output_file_name'])

import requests
import time
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY

headers = {'Authorization': API_KEY}
response = requests.get( API_URL + 'quota', headers = headers, verify=False)

print '\nAPI Aquota for:  ' + API_KEY + '\n'
print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response.text 


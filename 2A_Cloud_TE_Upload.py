import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY

print '\nUploading API\n'

headers = {'Authorization': API_KEY }

with open('malware/invoice.pdf','r') as sample:

	request = {"request": {"md5": "????",
						   "file_name": sample.name,
                      #"features": ["te"], .. by default
                      #Selecting no images means emulate with the default se
					  "te": {"reports": ["pdf","xml"],
                               "images": [
								          {"id": "5e5de275-a103-4f67-b55b-47532918fa59","revision": 1}, #Default Win7 32bit
                                          {"id": "e50e99f3-5963-4573-af9e-e3f4750b55e2","revision": 1}, #Default WinXP
										  {"id": "7e6fe36e-889e-4c25-8704-56378f0830df","revision": 1}, 
                                          {"id": "8d188031-1010-4466-828b-0cd13d4303ff","revision": 1},
                                          {"id": "3ff3ddae-e7fd-4969-818c-d5f1a2be336d","revision": 1},
                                          {"id": "6c453c9b-20f7-471a-956c-3198a868dc92","revision": 1}
										  ]}}}

	print request
	files = {'file':(sample),'request': json.dumps(request)}
	print '> Request for uploading a file with content-type multiform/metadata'
	response = requests.post( API_URL + 'upload', files=files ,
			     headers = headers, verify = False)

print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'
print response.text

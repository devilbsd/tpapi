import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY

report_uid = "2370f0cd-10a1-4325-9b6e-11e476277338"

print '\nDownloaing Forensic report for TE Analysis\n'


cookies = {}
cookies['te_cookie'] = 'BJAMBGKM'

response = requests.get( API_URL + 'download',
                          headers = {'Authorization': API_KEY },
			  params = {"id": report_uid},
			  cookies = cookies, ## This value will change!!
                          verify=False)


print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response

filename = report_uid + "_forensics_report.pdf"

with open (filename, mode = 'wb' ) as reportfile:
	reportfile.write(response.content)


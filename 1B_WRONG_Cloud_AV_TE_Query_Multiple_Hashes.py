import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY


headers = {'Authorization': API_KEY }
md5_list = ('841ad5dad3318940aab8798e192216c2',
            'df4a8675c095590b6a5f1c0f01b77e87',
            'c93b35f0d722c569ce9d9514475ac106',
            'c9862e64c6b417ab22423f3667bb1809',
            'bc5759cc2f07daf527b475cd9785be1e',
            'c94c52bac2dc0fcedccc727ccdf796c8',
            '50961213bc3e8f439521643fe34ef7a1',
            'c98bf7962ce48e2cd52326686f9abb64',
            '03dc86dcba10e4ace2360a727d88d32d')

request = { "request":  [
	{   "md5": md5_list,
	    "features": ["av","te"] }]}


print '\nQuery API\n'

print request

response = requests.post( API_URL + 'query',
			  data = json.dumps(request),
                          headers = headers,
                          verify=False)


responseText = json.loads(response.text)


print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response.text

#print type(responseText)


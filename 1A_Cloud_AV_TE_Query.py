import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY


headers = {'Authorization': API_KEY }

request = { "request":  [
							{   "md5": "841ad5dad3318940aab8798e192216c2",
								"features": ["av","te"]}
						]
		   }


print '\nQuery API\n'

print request

response = requests.post( API_URL + 'query',
			  data = json.dumps(request),
                          headers = headers,
                          verify=False)


responseText = json.loads(response.text)


print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response.text


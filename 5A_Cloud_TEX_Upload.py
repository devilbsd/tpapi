import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY
from api_config import API_COOKIE as API_COOKIE

print '\nUploading API for Threat Extraction\n'

headers = {'Authorization': API_KEY }


with open('documents/powerpoint.ppt','r') as sample:
	request = {"request": [{"md5": "???",
                            "file_name": "powerpoint.ppt",
                            "features": ["extraction"],
                            #"extraction": {"method": "clean"}
				}]}
	print request
	print ""

	files = {'file':(sample),'request': json.dumps(request)} 
        
        response = requests.post( API_URL + 'upload',
				files=files ,
				headers=headers,
				cookies = API_COOKIE,
				verify=False)

	print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'
	print response.text


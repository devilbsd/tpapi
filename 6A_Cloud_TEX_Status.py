import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY
from api_config import API_COOKIE as API_COOKIE

print '\nStatus Query API for Threat Extraction\n'

headers = {'Authorization': API_KEY }

request = {"request": {"md5": "8e89c7595271a2608c5be63f2de9c33e",
                        "file_name": "powerpoint.ppt",
                        "features": ["extraction"],
                        #"extraction": {"method": "pdf"}
				}}

print request
print ""
   
response = requests.post( API_URL + 'query',
					data=json.dumps(request) ,
					headers=headers,
					cookies = API_COOKIE,
					verify=False)

print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'
print response.text


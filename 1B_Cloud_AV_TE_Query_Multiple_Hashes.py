import requests
import json
from api_config import API_URL as API_URL
from api_config import API_KEY as API_KEY


headers = {'Authorization': API_KEY }

request = { "request":	[
						{"md5": "c9862e64c6b417ab22423f3667bb1809",
						"features": ["av","te"] },
						{"md5": "50961213bc3e8f439521643fe34ef7a1",
						"features": ["av","te"] }
						]
		   }


print '\nQuery API\n'

print request

response = requests.post( API_URL + 'query',
			  data = json.dumps(request),
                          headers = headers,
                          verify=False)


responseText = json.loads(response.text)


print '\nAPI Response by emulator: ' + response.cookies['te_cookie'] + '\n'

print response.text

#print type(responseText)

